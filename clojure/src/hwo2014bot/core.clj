(ns hwo2014bot.core
  (:require [clojure.data.json :as json]
            [clojure.set :as sets])
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message drained?]]
        [gloss.core :only [string]])
  (:gen-class)
  (:import (java.io File)))

; we need to contain channel here too since the connection gets broken randomly and we need
; to reconnect in such a case
(def initial-state {:running false
                    :bot-name nil
                    :track []
                    :lanes []
                    :channel nil
                    :tick-datas []})

(def state (atom initial-state))

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [message]
  (enqueue (:channel @state) (json/write-str message)))

(defn connect-client-channel! [{:keys [host port]}]
  (swap! state assoc :channel
         (wait-for-result
           (tcp-client {:host  host,
                        :port  port,
                        :frame (string :utf-8 :delimiters ["\n"])}))))

(defn read-message []
  (json->clj
    (try
      ; if the connection was b0rked -> reconnect
      (if (and (drained? (:channel @state)) (:running @state))
        (connect-client-channel! (:channel-params @state)))
      (wait-for-message (:channel @state))
      (catch Exception e
        (println (str "ERROR: " (.getMessage e)))
        (swap! state assoc :running false)
        "{}"))))

(defn car-datas [p msg]
  (->> msg
    :data
    (into #{})
    (sets/select #(p (get-in % [:id :name])))))

(defn own-car-data [msg]
  (first (car-datas #(= (:bot-name @state) %) msg)))

(defn throttle [amount]
  {:msgType "throttle" :data amount})

(defn straight? [track-piece]
  (= :straight (:type track-piece)))

(defn bend? [track-piece]
  (= :bend (:type track-piece)))

(defn car-piece-index [car-data]
  (get-in car-data [:piecePosition :pieceIndex]))

(defn car-lane [car]
  (get (:lanes @state) (get-in car [:piecePosition :lane :endLaneIndex])))

(defn as-piece-ind [ind]
  (cond
    (>= (count (:track @state)) ind) 0
    (< ind 0) (dec (count (:track @state)))
    :default ind))

(defn current-piece [car-data]
  (get (:track @state) (-> car-data car-piece-index as-piece-ind)))

(defn next-piece [car-data]
  (get (:track @state) (-> car-data car-piece-index inc as-piece-ind)))

(defn last-piece [car-data]
  (get (:track @state) (-> car-data car-piece-index dec as-piece-ind)))

(defn two-straight-pieces? [car]
  (and (straight? (current-piece car)) (straight? (next-piece car))))

(defn piece-length [piece car-lane]
  (cond
    (= :straight (:type piece)) (:length piece)
    (= :bend (:type piece)) (* 2
                               (Math/PI)
                               (+ (:radius piece) (:distanceFromCenter car-lane))
                               (/ (:angle piece) 360))
    :default (throw (RuntimeException. (str "Unknown piece: " piece)))))

(defn distance-to-end [car]
  (- (piece-length (current-piece car))
     (-> car :piecePosition :inPieceDistance)))

(defn bend-coming? [car]
  (and
    (straight? (current-piece car))
    (bend? (next-piece car))
    (< (distance-to-end car) 50)))

(defn bend-ending? [car]
  (and
    (bend? (current-piece car))
    (straight? (next-piece car))
    (< (distance-to-end car) 20)))

(defn inside-bend? [car]
  (and
    (bend? (current-piece car))
    (bend? (next-piece car))))

(defn slide-correction [car ret-msg]
  ; TODO: add real logic to compensate for too big slide angles
  (if (> (:angle car) 20)
    (throttle 0.1)
    ret-msg))

(defn own-car-data [msg]
  (first (car-datas #(= (:bot-name @state) %) msg)))

(defn same-piece? [car-1 car-2]
  (= (get-in car-1 [:piecePosition :pieceIndex])
     (get-in car-2 [:piecePosition :pieceIndex])))

(defn speed [car-old car-new]
  (if (or (nil? car-old) (nil? car-new))
    0
    (let [difference (- (get-in car-new [:piecePosition :inPieceDistance])
                        (get-in car-old [:piecePosition :inPieceDistance]))]
      (if (same-piece? car-old car-new)
        difference
        (+ difference (piece-length (current-piece car-old)))))))

(defmulti handle-msg :msgType)

(defmethod handle-msg "gameInit" [msg]
  (swap! state assoc :track (vec
                              (map
                                #(conj %
                                       (if (nil? (:length %))
                                         {:type :bend}
                                         {:type :straight}))
                                (-> msg :data :race :track :pieces))))

  (swap! state assoc :lanes (->> (get-in msg [:data :race :track :lanes])
                                 (sort-by :index)
                                 (map :distanceFromCenter)
                                 vec))
  {:msgType "ping" :data "ping"})

(defmethod handle-msg "carPositions" [msg]
  (let [own-car (merge (own-car-data msg) {:speed (speed (last (:tick-datas @state)) (own-car-data msg))})
        other-cars (car-datas #(not (= (:bot-name @state) %)) msg)]

    (swap! state update-in [:tick-datas] conj own-car)
    (slide-correction
      own-car
      (cond
        (two-straight-pieces? own-car) (throttle 1.0)
        (bend-coming? own-car) (throttle 0.5)
        (inside-bend? own-car) (throttle 0.5)
        (bend-ending? own-car) (throttle 0.75)
        :default (throttle 0.8)))))

(defmethod handle-msg "gameEnd" [msg]
  (swap! state assoc :running false)
  {:msgType "ping" :data "ping"})

(defmethod handle-msg :default [msg]
  {:msgType "ping" :data "ping"})

(defn log-msg [msg]
  (case (:msgType msg)
    "join" (println "Joined")
    "gameStart" (println "Race started")
    "crash" (printf "Crash! Speed: %s, Car angle: %s, Piece length: %s, Piece angle: %s\n"
                    (:speed (last (:tick-datas @state)))
                    (:angle (last (:tick-datas @state)))
                    (piece-length (current-piece (last (:tick-datas @state))))
                    (:angle (current-piece (last (:tick-datas @state)))))
    "gameEnd" (do
                (println "Race ended")
                (spit (File. "race-stats.clj") (select-keys @state [:track :tick-datas])))
    "error" (println (str "ERROR: " (:data msg)))
    :noop))

(defn game-loop []
  (let [msg (read-message)]
    (log-msg msg)
    (send-message (handle-msg msg))
    (when (:running @state)
      (recur))))

(defn -main[& [host port botname botkey]]
  (try
    (swap! state assoc :bot-name botname)
    (swap! state assoc :channel-params {:host host
                                        :port (Integer/parseInt port)})
    (swap! state assoc :running true)
    (connect-client-channel! (:channel-params @state))
    (send-message {:msgType "join" :data {:name botname :key botkey}})
    (game-loop)
    (catch Exception e
      (println (clojure.pprint/pprint (select-keys @state [:track :tick-datas])))
      (.printStackTrace e))))

(defn run-me []
  (-main
    "senna.helloworldopen.com"
    "8091"
    "SpeedyGonzales"
    "nta+9DiTMB+4RQ"))